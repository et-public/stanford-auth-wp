PATCH	?= 0
MINOR	?= 0
MAJOR	?= 0

# transform README.md into a WP-compatible readme.txt

readme.txt:	README.md
	@sed  <$< >$@ \
	     -e '/^<!--/d' -e '/^-->/d' \
			 -e 's/^# \(.*\)$$/== \1 ==/' \
	     -e 's/^## \(.*\)$$/== \1 ==/' \
	     -e 's/^### \(.*\)$$/= \1 =/'

patch:
	make version PATCH=1

minor:
	make version MINOR=1

major:
	make version MAJOR=1

version:
	old=$$( git tag -l | tail -n 1 | sed 's/^v//' ); \
    new=$$( echo $$old | awk -F. '{printf("%d.%d.%d", $$1+maj,$$2+min,$$3+p)}' maj=$(MAJOR) min=$(MINOR) p=$(PATCH) ); \
	  sed -i.bak "s/Stable tag: $$old/Stable tag: $$new/" README.md; \
	  sed -i.bak "s/^ \* Version: $$old/ * Version: $$new/" stanford-auth.php; \
		$(RM) README.md.bak stanford-auth.php.bak; \
	  git commit -m "Bumped version to $$new" README.md stanford-auth.php; \
	  git tag v$$new

clean:
	$(RM) readme.txt *.bak
