# Stanford Authentication

<!--
Contributors: scottylogan
Donate link: https://giving.stanford.edu
Tags: authentication, SAML, Stanford, SSO
Requires PHP: 7.0
Requires at least: 5.0
Tested up to: 5.4
Stable tag: 0.1.5
License: MIT
License URI: https://opensource.org/licenses/MIT
-->

Stanford-specific SAML authentication for WordPress, leveraging
[Pantheon](https://pantheon.io/)'s [WP SAML
Auth](https://wordpress.org/plugins/wp-saml-auth/).

## Description

This plugin configures [Pantheon](https://pantheon.io/)'s [WP SAML
Auth](https://wordpress.org/plugins/wp-saml-auth/) plugin to work with
Stanford University's SAML IdP; while this plugin configures many of
the WP-SAML-Auth settings, it also needs some additional site-specific
configuration.

## Frequently Asked Questions

### Does it work with WordPress Multisite?

Not yet... but I'm working on it

### Can I configure it through the WordPress console?

Not yet. At this time you need to write your own plugin to configure the Stanford plugin, which in turn configures the WP SAML Auth plugin.

### Can I federate my site with multiple Identity Providers?

Not at this time.

<!--
## Screenshots

1. Plugins Settings - you should have your custom configuration (_IT Arch Configuration_ in this site), _Stanford Authentication_, and _WP SAML Auth_ enabled.

2. Once everything is working, you should not be able to configure the WP SAML Auth plugin.

3. The _SAML Metadata_ option under _Settings_ should show valid SAML metadata and links to [SPDB](https://spdb.stanford.edu).
-->

<!--
## Upgrade Notice

TBD
-->

## Changelog

### 0.1.5

* Updated instructions for generating the keypair for SAML
* Made readme.txt "WP compliant"; added Makefile

### 0.1.4

* Fixed `fix_role_before_insert_user` function name
* Show invalid metadata on metadata page; previously it would just show an error stating that the metadata was invalid
* fix code.stanford.edu URL in `stanford-auth.php`

## Installation

Since this plugin configures WP SAML Auth, start by installing, but *not configuring*, [WP SAML Auth](https://wordpress.org/plugins/wp-saml-auth/).

Next, install this plugin. Currently, it's easiest to do via `git`:

```shell
% cd /my/wordpress/base/wp-content/plugins
% git clone https://code.stanford.edu/et-public/stanford-auth-wp.git
% ls -F stanford-auth-wp/
Makefile                      example-stanford-auth-config/ readme.txt
README.md                     inc/                          stanford-auth.php
%
```

Finally, copy the example config to the plugins directory and rename it:

```shell
% cp -r stanford-auth-wp/example-stanford-auth-config stanford-auth-config
% cd stanford-auth-config
% ls -F
example-stanford-auth-config.php  private/
% mv example-stanford-auth-config.php stanford-auth-config.php
% ls -F
stanford-auth-config.php  private/
% cat private/.htaccess
<Files "sp.(crt|key)">
  Require all denied
</Files>

%
```

You can now edit `stanford-auth-config/stanford-auth-config.php` to configure the Stanford Auth plugin.

### Configuration

Edit `stanford-auth-config.php` and change all occurrences of `mysite` to a short name for your site (i.e. the _name_ part of _https://name.stanford.edu_. Here's an example for [https://itarch.stanford.edu/]:

```php
<?php
/**
 * Plugin Name: IT Arch Stanford Authentication Configuration
 * Version: 0.1.0
 * Description: IT Arch configuration for Stanford Auth plugin
 * Author: John Doe
 * Author URI: https://itarch.stanford.edu
 * Plugin URI: https://code.stanford.edu.org/et-public/itarch/
 *
 * @package Stanford_Auth_Config
 */

function itarch_auth_options( $default_value, $option_name ) {
  $options = array(
    'attribute_map' => array (
      'entitlement' => array (

        // map members of itlab:itarch_admins workgroup to WP administrator role
        'itlab:itarch_admins'   => 'role:administrator',

        // map members of itlab:itarch_authors workgroup to WP author role
        'itlab:itarch_authors'  => 'role:author',

        // map anyone in UIT to WP subscriber role
        'uit:all'               => 'role:subscriber',
      ),
      'affiliation' => array (

        // map anyone at Stanford an anonymous WP user
        'member@stanford.edu' => 'user:anonymous',
      ),
    ),

    // out setup passes the EntityID through an environment variable
    'entityId' => $_SERVER['ENV_ENTITY_ID'],

    // Since we have acccess to the system running WordPress, we can
    // store the SAML public certificate and private key in a local 
    // directory outside the wordpress tree, which is more secure.
    'sp_cert_file' => '/etc/saml/itarch.pem',
    'sp_key_file'  => '/etc/saml/itarch.key',

    // only SAML logins are allowed
    'permit_wp_login' => false,
  );

  return isset( $options[ $option_name ] ) ? $options[ $option_name ] : $default_value;
}

add_filter( 'stanford_auth_option', 'itarch_auth_options', 0, 2 );
```

### Configuring the SAML Certificate and Key

Stanford's IdP requires encrypted SAML assertions by default, so
unless you request an exemption, you will need to create a public
certificate and private key for decrypting those assertions.

SAML certificates and keys are separate from the certificate and key
used for HTTPS on your server, and UIT recommends creating a
"self-signed" keypair. You can use the `openssl` command (usually installed on Linux and MacOS/macOS systems), replacing `itarch.stanford.edu` with the host
part of your entityID, or the host part of your site's URL
(e.g. `mysite.stanford.edu`):

```shell
% openssl req -x509 -nodes -newkey rsa:2048 -keyout sp.key -out sp.crt -days $((365*20)) -subj /CN=itarch.stanford.edu
Generating a 2048 bit RSA private key
...........+++
............................................+++
writing new private key to 'sp.key'
-----
% ls -l sp.*
-rw-r--r--  1 swl  staff  1123 Jun  4 13:03 sp.crt
-rw-r--r--  1 swl  staff  1704 Jun  4 13:03 sp.key
% openssl x509 -in sp.crt -noout -subject -dates
subject= /CN=itarch.stanford.edu
notBefore=Jun  4 20:03:52 2020 GMT
notAfter=May 30 20:03:52 2040 GMT
%
```

### SAML Credentials Security

Your SAML credentials (certificate and key) need to be stored
securely. How to do this varies depending on how much access you have
to the system(s) hosting your WordPress site.

#### Cloud Provider Secrets Service

If your site runs on a cloud provider like Amazon Web Services, Google
Cloud Platform, or Microsoft Azure, you can store your credentials in
their secrets store service, and download the credentials whenever
your WordPress server(s) restart. While you'll need credentials to
access the secrets store, most cloud providers have a way to assign a
role with suitable permissions to the hosting platform.

#### Environment Variables

If your SAML key and certificate are available as environment
variables, you can also use a single file. Update the plugin settings
to set `sp_cert` and `sp_key` (not `sp_cert_file` and `sp_key_file`)
to the full filesystem paths of your certificate and key:

```php
    'sp_cert' => $_ENV['MY_CERT_ENV_VAR'],
    'sp_key'  => $_ENV['MY_KEY_ENV_VAR'],
```

#### External Filesystem

My site, [itarch](https://itarch.stanford.edu) runs as multiple [Docker](https://docker.com) containers on a cluster with a shared filesystem, so the SAML credentials are stored in a directory on the shared filesystem; the directory is mounted into each container at `/etc/saml/`.

#### Local Filesystem

Using a local filesystem is similar to using an external filesystem, and may be necessary if you have no shared filesystem, or if you only have a single server. You need access to upload the credentials to a directory on the server outside the WordPress directory.

#### WordPress Directory

While the least desirable, it may be the only option for many
providers. If you are using a copy of the
`example-stanford-auth-config` for your configuration plugin there is
already a subdirectory in that plugin called `private` that contains
an `.htaccess` file that blocks all web access to the directory
contents:

```
<Files "sp.(crt|key)">
  Require all denied
</Files>
```

The example configuration shows how to reference those files:

```php
    'sp_cert_file' => plugin_dir_path( __FILE__ ) . 'private/sp.crt',
    'sp_key_file'  => plugin_dir_path( __FILE__ ) . 'private/sp.key',
```

#### WordPress Database

Lastly, you could store the credentials in the WordPress database, and modify your `site_auth_config` plugin to extract the credentials and use them like environment variables. Here's an 

```php
function get_my_saml_cert() {
  // query database for cert, return PEM formatted string
}

function get_my_saml_key() {
  // query database for key, return PEM formatted string
}

function my_stanford_auth_options( $default_value, $option_name ) {
  $options = array(
    // ...
    'sp_cert' => get_my_saml_cert(),
    'sp_key'  => get_my_saml_key(),
    // ...
  );
  // ...
}
```

(This is the probably the method we'll use for site-specific SAML credentials in a WordPress Multisite environment.)

### Other Configuration Options

The default entity ID for your site is `https://` + the hostname (as determined by PHP / WordPress). You can override this by setting the `entityId` option.

```php
    'entityId' => 'http://wp-test.itlab.stanford.edu',
```

You can disable WordPress form authentication by setting the `permit_wp_login` option to `false` - but please ensure SAML authentication is working properly before doing that.

### SAML Attribute to Role and User Mapping

The `attribute_map` option enables mapping Stanford users (directly, or via [Workgroup](https://workgroup.stanford.edu)  memberships to WordPress roles, or to specific WordPress users, using attributes received from the IdP.

The most common use of this is to map
(workgroups)[https://workgroup.stanford.edu/] to roles. Stanford
Workgroups are released as `eduPersonEntitlement` attributes, usually
called `entitlement`:

```php
    'attribute_map' => array (
      'entitlement' => array (
        'itlab:staff'  => 'role:administrator',
        'test:staff'   => 'role:author',
        'test:student' => 'role:contributor',
      ),
    ),
```

Additionally, many people can be mapped to existing WordPress users
based on their Stanford workgroup memberships:

```php
    'attribute_map' => array (
      'entitlement' => array (
        'test:faculty' => 'user:general-user',
      ),
    ),
```

Or, using the `affiliation` attribute (also known as
`eduPersonScopedAffiliation`) you could map all students to a single
user:

```php
    'attribute_map' => array (
      'affiliation' => array (
        'student@stanford.edu' => 'user:general-user',
      ),
    ),
```

Individual users can be mapped to roles (or even other users) using the
`eppn` or `uid` attributes:

```php
    'attribute_map' => array (
      'eppn' => array (
        'leland@stanford.edu'   => 'role:administrator',
        'jane@stanford.edu'     => 'role:author',
        'lelandjr@stanford.edu' => 'user:lelandjr',
      ),
      'uid' => array (
        'leland'  => 'role:administrator,
        'jane     => 'role:author',
        'lelandjr => 'role:subscriber',
      ),
    ),
```
